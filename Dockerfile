FROM node:8.12.0-jessie

RUN apt update && apt install python

RUN mkdir coresciences

COPY . ./solinr.net

RUN cd solinr.net && \
      npm install && \
      npm install gatsby-cli && \
      node_modules/.bin/gatsby build

EXPOSE 80/tcp

CMD cd solinr.net && node_modules/.bin/gatsby serve -p 80
