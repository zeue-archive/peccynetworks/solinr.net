module.exports = {
  siteMetadata: {
    title: "Solinr Hosting | by Peccy Networks",
    author: "Alex (@zeue)",
    description: "EMPOWERING BUSINESSES WITH CLOUD TECHNOLOGY"
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'solinr-net',
        short_name: 'solinr',
        start_url: '/',
        background_color: '#003cc8',
        theme_color: '#003cc8',
        display: 'minimal-ui',
        icon: 'src/images/logoWhiteOnBlueCircle.png',
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-offline'
  ],
}
